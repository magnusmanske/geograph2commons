/*
  (c) 2013 by Magnus Manske
  released under GPL V2+
*/

function runTool () {
	var server = $('#server').val() ;
	var photo_id = $('#photo_id').val() ;
	var categories = $('#categories').val().split("\n") ;
	if ( photo_id == '' ) {
		alert ( "At least gimme a photo ID, sir!" ) ;
		return ;
	}


	$.getJSON ( "g2c.php" , { server:server , photo_id:photo_id } , function ( d ) {
		var desc = $($(d.reuse).find("#wikipedia textarea")[0]).text() ;
		if ( desc == '' ) { alert ( "Could not get description - check photo ID" ) ; return }
		desc += "\n\n" ;
		$.each ( categories , function ( k , v ) {
			if ( v != '' ) desc += "[[Category:" + v + "]]\n" ;
		} ) ;
		desc = $.trim ( desc ) ;

		var title = [] ;
		$.each ( $(d.reuse).find("h2 a") , function ( k , v ) {
			v = $(v).text() ;
			if ( v != '' ) title.push ( v ) ;
		} ) ;
		title = title[1].replace(/'/g,'') + " (geograph " + photo_id + ").jpg" ;
		title = title.replace(/\s+/,' ');

		let new_filename = $('#new_filename').val();
		if ( new_filename != '' ) title = new_filename ; // Override
		else $('#new_filename').val(title);

		var hires_url = $($(d.main).find('#mainphoto img')).attr('src') ;
		$.each ( $(d.main).find('a[rel="license"]') , function ( k , v ) {
			var about = $(v).attr('about') ;
			if ( undefined === about ) return ;
			var m = about.match ( /\/\d+_([0-9a-f]+)\.jpg/ ) ;
			if ( m == null ) return ;
			hires_url = "https://www.geograph.org.uk/reuse.php?id=" + photo_id + "&download=" + m[1] + "&size=largest" ;
		} ) ;
		
		title = title.replace ( /:/g , '-' ) ;

		desc = desc.replace ( /http:/g , 'https:' ) ;

		var h = "<div>Uploading image as \"" + title + "\"</div>" ;
		h += "<pre>" + desc + "</pre>" ;
		h += "<div><a href='" + hires_url + "'>Hi-Res image</a></div><hr/>" ;
		h += "<div id='uploading'><i>Uploading...</i></div>" ;
		$('#result_container').show().html(h);

		transferFile ( {
			url : hires_url ,
			name : title ,
			comment : "Transferred from geograph.co.uk using [https://tools.wmflabs.org/geograph2commons/ geograph2commons]" ,
			desc : desc
		} ) ;

/*
		var params = {
			tusc_user : tusc.user ,
			tusc_password : tusc.pass ,
			url : hires_url ,
			new_name : title ,
			hotfix : 1 ,
			source : 'Geograph' ,
			desc : desc
		} ;
	
		$.post ( '/magnustools/php/upload_from_url.php' , params , function ( d2 ) {
			var tr = "" ;
			if ( d2.status == 'OK' ) {
				tr = "Now at : <a target='_blank' href='//commons.wikimedia.org/wiki/File:" + escape ( title ) + "'><tt>" + title + "</tt></a>" ;
			} else {
				var s = d2.status ;
				if ( d2.note !== undefined ) s += " (" + d2.note + ")" ;
				tr += "<br/><b>Transfer failed : " + s + "</b>" ;
			}
			$('#uploading').html ( tr ) ;
		} , 'json' ) ;
*/

	} ) ;
	
}

function transferFile ( o ) {

	function urldecode(url) {
	  return decodeURIComponent(url.replace(/\+/g, " "));
	}

	var params = {
		action:"upload_from_url",
		newfile:o.name,//urldecode("'.urlencode($newname).'"),
		url:o.url,
		desc:o.desc,//urldecode("'.urlencode($upload_text).'"),
		comment:o.comment,
		botmode:1
	} ;

	function showError ( msg , d ) {
		var h = "<big>ERROR: "+msg+"</big>" ;
		if ( typeof d != 'undefined' ) {
			if ( typeof d.res.upload.warnings != 'undefined' ) {
				if ( typeof d.res.upload.warnings.duplicate != 'undefined' ) h += "<div>File already on Commons as:<br/>" + d.res.upload.warnings.duplicate[0] + "</div>" ;
				else h += "<div>" + JSON.stringify(d.res.upload.warnings) + "</div>" ;
			}
		}
		$("#uploading").html ( h ) ;
	}
	
	$.post ( "/api.php" , params , function ( d ) {
		if ( d.error != "OK" ) {
			console.log ( d ) ;
			showError ( d.error , d ) ;
			return ;
		}
		
		var tr = "<big>Success!</big><br/>Now at : <a target='_blank' href='//commons.wikimedia.org/wiki/File:" + encodeURIComponent ( o.name ) + "'><tt>" + o.name + "</tt></a>" ;
		$('#uploading').html ( tr ) ;
//		$("#uploading").html ( "<big>Transfer successful!</big>" ) ;
//		$("#newfile_link").attr("href","//commons.wikimedia.org/wiki/File:"+params.newfile) ;
//		$("#now_commons_buttons").show() ;

		// Logging
		$.getJSON ( 'https://tools.wmflabs.org/magnustools/logger.php?tool=geograph2commons&method=upload to commons&callback=?' , function(j){} ) ;

	} , "json" ) .fail(function() { showError ( "API failed or does not respond" ) } ) ;
}

function checkOAuth ( callback ) {
	$.get ( '/api.php' , {
		action:'get_rights',
		botmode:1,
		site:'commons.wikimedia.org'
	} , function ( d ) {
		if ( d.error == 'OK' && typeof d.result.error == 'undefined' ) {
			$('#oauth').addClass('alert alert-success').html ( '<button type="button" class="close" data-dismiss="alert">&times;</button>'+'You have successfully authorised this tool. You\'re good to go!' ) ;
			$('#b_run').show();
		} else {
			$('#oauth').addClass('alert alert-error').html ( '<button type="button" class="close" data-dismiss="alert">&times;</button> <a href="/api.php?action=authorize">Log in</a> to transfer files!' ) ;
		}
		callback() ;
	} , 'json' ) ;
}

$(document).ready ( function () {
	loadMenuBarAndContent ( { toolname : 'Geograph2Commons' , meta : 'Geograph2Commons' , content : 'form.html' , run : function () {
		checkOAuth ( function () {
//		tusc.setupLoginBar ( $('#tusc_container_wrapper') , function () {
			wikiDataCache.ensureSiteInfo ( [ { lang:'commons' , project:'wikimedia' } ] , function () {
	
				$('#toolname').html ( "Geograph-to-Commons" ) ;
//				tusc.initializeTUSC () ;
//				tusc.addTUSC2toolbar() ;
				$('#photo_id').focus() ;
				
				
				$('#server').tooltip ( { placement : 'right' , trigger: 'hover' } ) ;
				$('#photo_id').tooltip ( { placement : 'right' , trigger: 'hover' } ) ;
				$('#categories').tooltip ( { placement : 'right' , trigger: 'hover' } ) ;
				$('#new_filename').tooltip ( { placement : 'right' , trigger: 'hover' } ) ;

				var params = getUrlVars() ;
//				if ( undefined !== params.tusc_user ) $('#tusc_user').val ( decodeURIComponent(params.tusc_user) ) ;
//				if ( undefined !== params.tusc_pass ) $('#tusc_pass').val ( decodeURIComponent(params.tusc_pass) ) ;
				if ( undefined !== params.server ) $('#server').val ( decodeURIComponent(params.server) ) ;
				if ( undefined !== params.photo_id ) $('#photo_id').val ( decodeURIComponent(params.photo_id) ) ;
				if ( undefined !== params.categories ) $('#categories').val ( decodeURIComponent(params.categories) ) ;
				
			} ) ;
		} ) ;
	} } )
} ) ;
